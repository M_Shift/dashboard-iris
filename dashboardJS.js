var mapsArray = new Array(); // Array used to hold instance of leaflet maps
            var mapCounts = 0; // total amout of maps in the mapArray
            var currentMapcount = 0; // when connecting to new dashbaords stores the staring point for any maps added to the map array
            var mapSettings = new Array(); // sister Array to mapsArray, stores variables and layers in creating corresponding map in Maps Array
            var ChartsArray = new Array(); // Array used to hold instances of echarts charts
            var chartSettings = new Array(); // Sister Array to ChartsArray, holds variables used to create chart in corresponding chartArry;
            var currentChartCount =0; // wehen connecting to a new database this holds the count for the where new charts are added to ChartsArray,  needed for additional filter function
            var barCharts = 0; // number of charts in the Charts array
            var datafeildCount; // number of charts in the chart array.
            var returndata; // holds the json data returned from the httpxml request


            var requestURL = 'https://data.medicaid.gov/resource/f8sh-7iwd.json?'; // default database request
                var request = new XMLHttpRequest();

                request.open('GET',requestURL);
                request.responseType = 'json';

                request.send();
                request.onload = function getData(data){


                returndata = request.response;

                datafeildcount = Object.keys(returndata[0]);

                for (var i = 0; i < 100 && i < returndata.length ; i++){
                  if(datafeildcount.length < Object.keys(returndata[i]).length)
                  datafeildcount = Object.keys(returndata[i]);
                }


                updateFeilds(datafeildcount);
                };

              /*function title for the dashboard*/
              function dashboardTitle(){
                var titleText = document.getElementById("DashboardTitle").value;
                document.getElementById("DashTitle").innerHTML = titleText;
              }

            /*executes when URL is submitted by the user,  connects to json requests from databases*/
            function submitXMLURL(URLtext){  //when you click the getrock button this gets executed

                alert(URLtext); //text box appears for debugging
                currentMapcount =  mapCounts; //counter for map filers in map array
                currentChartCount = barCharts;

                 //variable to store returndata

                var requestURL =  URLtext; // assigns URLtext to variable
                var request = new XMLHttpRequest();

                request.open('GET',requestURL); //retrieves data (opening)
                request.responseType = 'json';  //expecting a json format

                request.send(); // sending requst
                request.onload = function getData(data){ //once data recieved excute following code below
                    returndata = request.response;

                    datafeildcount = Object.keys(returndata[0]); //taking first row of data and counting cols -therefore testing bug


                    datafeildcount = Object.keys(returndata[0]);

                    for (var i = 0; i < 100 && i < returndata.length ; i++){
                      if(datafeildcount.length < Object.keys(returndata[i]).length)
                      datafeildcount = Object.keys(returndata[i]);
                    }

                    updateFeilds(datafeildcount); //custom method to update datfield
                };
            }


            /*adds buttons for each coloumn or datafeild selection on the side*/
            function updateFeilds(feildNums) {

                var count = 0;
                var deleteButtons = document.getElementsByClassName('datafield');

                if(document.getElementById("myTable") != null  ){
                    for(var i = document.getElementById("myTable").rows.length; i > 0;i--) {
                        document.getElementById("myTable").deleteRow(i -1);
                    }
                }

                if (deleteButtons[0] != null){
                  for(var i = 0 ; deleteButtons[0] != null; i++){
                  deleteButtons[0].parentNode.removeChild(deleteButtons[0]);
                  document.getElementById("dataValuesTable").deleteRow(i -1);

                  }
                }
                for(var i = 0; i < feildNums.length; i++){
                  var btn = document.createElement("BUTTON");
                  btn.id ='someId'+i;
                  btn.className= 'datafield';

                  var t = document.createTextNode(feildNums[i]);
                  btn.appendChild(t);
                  document.body.appendChild(btn);

                  var element = document.getElementById('someId'+i);
                  element.onclick = createList;
                  var table = document.getElementById("myTable");
                  var row = table.insertRow(count);
                  var cell1 = row.insertCell(0);
                  cell1.appendChild(btn);
                  count++;
                }
                generateMenu(feildNums);// populates the dropdown list.
            }


            /* creates array of unique values from returned data based on the buttonpressed*/
            function createList(buttonPressed){
              var allData =  new Array();
              var sortedDupsData;
              var keyname = buttonPressed.target.innerText;

              for(var i =0; i < returndata.length; i++){
                allData.push(returndata[i][keyname]);
              }

              allData.sort();
              sortedDupsData = removeDuplicates(allData);
              document.getElementById('filterButton').key = buttonPressed.target.innerText;
              updateFeilds2(sortedDupsData);
            }

            /*sort numeric numbers becuase javascript is stupid*/
            function sortNumeric(array) {
                array.sort(function(a, b){return a-b});
            }


            /*this function removes duplicaes in an array and returns a fresh array*/
            function removeDuplicates(arr){
                let unique_array = [];
                for(let i = 0;i < arr.length; i++){
                    if(unique_array.indexOf(arr[i]) == -1){
                        unique_array.push(arr[i])
                    }
                }
                return unique_array;
            }

            /*this function creates a list of values when a column button is pressed*/
            function updateFeilds2(feildNums) {

                for(var i = document.getElementById("dataValuesTable").rows.length; i > 1;i--) {
                    document.getElementById("dataValuesTable").deleteRow(i -1);
                }
                var count = 1;

                for(var i = 0; i < feildNums.length; i++){
                  var checkBox = document.createElement("INPUT");
                  checkBox.setAttribute("type", "checkbox");
                  checkBox.id ='cBox'+i;
                  checkBox.className= 'checkboxs_filter';
                  checkBox.key = feildNums[i];

                  var label = document.createElement('label')
                  label.htmlFor = 'cBox'+i;
                  label.appendChild(document.createTextNode(feildNums[i]));
                  document.body.appendChild(checkBox);

                  var table = document.getElementById("dataValuesTable");
                  var row = table.insertRow(count);
                  var cell1 = row.insertCell(0);
                  cell1.appendChild(checkBox);
                  cell1.appendChild(label);
                  count++;
                }
              }

              /*checks to see if string value is a number*/
              function isNumeric(n) {
                  return !isNaN(parseFloat(n)) && isFinite(n);
                }

            /*makes sure that some sort of string has been entered in the input box*/
            function validateForm() {
                var dataBase =  document.getElementById("URL_text").value;
                if (dataBase== "") {
                alert(dataBase);
                    return false;
                }
                var playData = submitXMLURL(dataBase);
                // https://data.cityofchicago.org/resource/f7f2-ggz5.json?  murder data
            }



          function upDateInstruction(cat, val, submit){
            document.getElementById('instructCat').innerHTML = cat;
            document.getElementById('instructVal').innerHTML = val;
            document.getElementById('instructSubmit').innerHTML = submit;
          }

          /*creates div space for the chart*/
          function creatediv(chartId){

             var chart = document.createElement("div");
             chart.setAttribute("id",chartId);
             chart.setAttribute("class","chartclass");

             var closing = document.createElement("div");
             closing.setAttribute("id",'testip');
             closing.setAttribute("class","container");
             closing.innerHTML= "&times;";
             document.body.appendChild(closing);
             document.body.appendChild(chart);

             //document.body.appendChild(closing);
          }

          /* opens the graph creation menu when Bar button is clicked*/
          function createBar(buttonPressed){
            var f = document.getElementById('myModal');
            document.getElementById('catBtn').innerHTML = "xAxis";
            document.getElementById('valBtn').innerHTML = "yAxis";
            upDateInstruction("Select Category For Chart",
                             "Select Value that is charted",
                             "Create Chart");
            f.key = "bar";
            boxOptions();
          }

          /* opens the graph creation menu when Line button is clicked*/
          function createLine(buttonPressed){
            var f = document.getElementById('myModal');
            document.getElementById('catBtn').innerHTML = "xAxis";
            document.getElementById('valBtn').innerHTML = "yAxis";
            upDateInstruction("Select Category For Chart",
                             "Select Value that is charted",
                             "Create Chart");
            f.key = "line";
            boxOptions();
          }

          /* opens the graph creation menu when pie button is clicked*/
          function createPie(buttonPressed){
            var f = document.getElementById('myModal');
            document.getElementById('catBtn').innerHTML = "Category";
            document.getElementById('valBtn').innerHTML = "Values";
            upDateInstruction("Select Category For Chart",
                             "Select Value that is charted",
                             "Create Chart");
            f.key = "pie";
            boxOptions();

          }

          /* opens the graph creation menu when map button is clicked*/
          function createMap(buttonPressed){
            var f = document.getElementById('myModal');
            document.getElementById('catBtn').innerHTML = "Latitude";
            document.getElementById('valBtn').innerHTML = "Longitude";
            upDateInstruction("Select Latitude Variable",
                             "Select Longitude Variable",
                             "Create Map");
            f.key = "map";
            boxOptions();

          }

          // When the user clicks the button, open the modal
          function boxOptions() {
              document.getElementById('myModal').style.display = "block";
          }

          // When the user clicks on <span> (x), close the modal
          function closebox(){
                document.getElementById('myModal').style.display = "none";
          }


          /* When the user clicks on the xAxis or category button,
          toggle between hiding and showing the dropdown content */
          function dropboxOpen(elm) {
              document.getElementById("myDropdown").classList.toggle("show");
          }

          /* When the user clicks on the yAxis or value button,
          toggle between hiding and showing the dropdown content */
          function dropboxOpenYaxis(elm){
            document.getElementById("myDropdownYaxis").classList.toggle("show");
          }

          /* creating list for dropdown menu*/
          function generateMenu(list) {

            var drop1 = document.getElementById("myDropdown");
            var drop2 = document.getElementById("myDropdownYaxis");

            while (document.getElementById("myDropdownYaxis").childNodes.length > 0 ){
              drop2.removeChild(drop2.childNodes[0]);

            }

            while (document.getElementById("myDropdown").childNodes.length > 0){
              drop1.removeChild(drop1.childNodes[0]);
            }

            for (var i = 0; i< list.length; i++){
              var a = document.createElement('a');
              var b = document.createElement('a');

              var t = document.createTextNode(list[i]);
              a.appendChild(t);
              var u = document.createTextNode(list[i]);
              b.appendChild(u);
              a.class = "cat_Selections";
              b.class = "val_Selections";
              document.getElementById("myDropdown").appendChild(a);
              document.getElementById("myDropdownYaxis").appendChild(b);
              a.onclick = setValueForGraph;
              b.onclick = setValueForGraph;
            }
          }

          /*drop down lists for the pop up chart options popup*/
          function setValueForGraph(t) {

            if(t.target.class == 'cat_Selections'){
              document.getElementById('catBtn').textContent = t.target.text;
              document.getElementById("myDropdown").classList.remove("show");
              return;
            }
            if(t.target.class = 'val_Selections'){
              document.getElementById('valBtn').textContent = t.target.text;
              document.getElementById("myDropdownYaxis").classList.remove("show");
              return;
            }
          }

          /*this shall draw a mao on the page*/
          function createMapChart(elm, mapCounts, returnData, chartId){

            var latitude = document.getElementById('catBtn').textContent;
            var longitude = document.getElementById('valBtn').textContent;
            var latArray = new Array();
            var longArray = new Array();
            var latAve = 0;
            var longAve = 0;
            var count = 0;
            for (var i  = 0; i < returnData.length; i++){
              if(returnData[i][latitude] != null){
                latArray.push(returnData[i][latitude]);
                latAve += parseFloat(returnData[i][latitude]);
                longArray.push(returnData[i][longitude]);
                longAve += parseFloat(returnData[i][longitude]);
                count++;
              }
            }

            document.getElementById('myModal').style.display = "none";
            try{
            if(count > 0){
              latAve = latAve/count;
              longAve = longAve/count;
            }
            //initate the map
            mapsArray.push(L.map(chartId).setView([latAve,longAve], 5));
                L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFydHloIiwiYSI6ImNqanhvZXhkdDFnM2EzcnJyaGpqMzRwdTgifQ.fdsKRyEOOsYc8jstIJPIlw', {
                    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                    maxZoom: 18,
                    id: 'mapbox.streets',
                    zIndex : 0,
                    accessToken: 'pk.eyJ1IjoibWFydHloIiwiYSI6ImNqanhvZXhkdDFnM2EzcnJyaGpqMzRwdTgifQ.fdsKRyEOOsYc8jstIJPIlw'
                }).addTo(mapsArray[mapCounts]);
            //draw circles on the map
              var markerGroup = L.layerGroup().addTo(mapsArray[mapCounts]);
              for (var i=0; i<latArray.length; i++){
                var circle = L.circle([latArray[i], longArray[i]], {
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 0.5,
                    radius: 300
                }).addTo(markerGroup);
              }
                var set = {lat:latitude, long:longitude, layer:markerGroup};
                mapSettings.push(set);
                //circle.bindPopup("I am a state code " + pulledData[i]["state_code"]);

            }catch{
                alert("latitude and longitude data not relevant");
                document.getElementById(chartId).remove();
                }

                for(var i = 0; i < mapSettings.length; i++){
                }


            //markerGroup.remove();
              //layer.remove();
              //mapsArray[mapCounts].removeLayer(layer)



          }

          /*when the submit button in popup boxes is pressed this will run
          it checks to see if line, bar, pie chart has been pressed and will crerate accordingly*/
          function createGraph(elm){
            var key = document.getElementById('myModal').key;
            var title = document.getElementById('chartTitle').value;
            if(key == "map"){
              creatediv("map"+mapCounts);
              createMapChart(elm, mapCounts, returndata, "map"+mapCounts );
              this.mapCounts++;
              return;
            }
            creatediv("bar"+barCharts);
            ChartsArray[barCharts] = echarts.init(document.getElementById(("bar"+barCharts)));
            var category = document.getElementById('catBtn').textContent;
            var categoryArray = createCategory(category, returndata);

            var value = document.getElementById('valBtn').textContent;
            var set = {cat:category, val:value };
            chartSettings.push(set);
            var valueArray = createValues(value, categoryArray, category, returndata);
            document.getElementById('myModal').style.display = "none";
            if(key == 'pie'){
              drawPie(categoryArray, valueArray, key,title);
              document.getElementById('myModal').key = "";
              return;
            }

            var option = {
                title: {
                  text: title,
                  left: 'center'
                },
                tooltip: {},
                legend: {
                    data:[category]
                },
                xAxis: {
                    data: categoryArray,
                    name: category,

                    nameLocation: "middle",

                },
                yAxis: {
                  name: value,
                  nameLocation: "middle",
                  nameGap: 60,
                  axisLabel: {
                      rotate: 30
                  }
                },
                grid: {
                    left: 100
                },
                series: [{
                    name: value,
                    type: key,
                    data: valueArray,

                }],
                toolbox: { //this sets download options for the graph
                  right: '10%',
                  feature: {
                      saveAsImage: {
                        title: "Save as Image"
                      }
                  }
                },
            };
            ChartsArray[barCharts].setOption(option);
            barCharts++;
            document.getElementById('myModal').key = "";
          }

          /*creates the category array*/
          function createCategory(category, dataArray){
            var allData =  new Array();
            var sortedDupsData;

            for(var i =0; i < dataArray.length; i++){
              allData.push(dataArray[i][category]);
            }
            allData.sort();
            sortedDupsData = removeDuplicates(allData);
            return sortedDupsData;
          }

          /*creates the values that are charted in the graphs*/
          function createValues(value, categoryArray,category, dataArray){
            var valueArray = new Array();

            for(var i =0; i < categoryArray.length; i++){
              valueArray.push(0);
            }
            //checks against category array to see if the category is the same and adds that to the value array
            for(var i = 0; i < categoryArray.length; i++){
              var key = categoryArray[i];
              for(var j = 0; j < dataArray.length; j++) {
                if(dataArray[j][category] === key && dataArray[j][value]  > 0 && key!= "XX" ) {
                  valueArray[i] += parseFloat(dataArray[j][value]);
                }
              }
            }
            return valueArray;
          }

          /*creates pie chart when the submit button is pressed*/
          function drawPie(categoryArray, valueArray, key,title){

            var pieData = new Array();
            ChartsArray[barCharts] = echarts.init(document.getElementById(("bar"+barCharts)));

            for(var i=0; i< categoryArray.length; i++){
              pieData.push({value: valueArray[i], name: categoryArray[i]});
            }

            // specify chart configuration item and data
            var option = {
                title: {
                    text: title
                },
                toolbox: { //this sets download options for the graph
                    feature: {
                      saveAsImage: {
                        title: "Save as Image"
                      }
                    }
                },
                series: [{
                    name: 'Sales',
                    type: 'pie',
                    data: pieData

                }]
            };
            ChartsArray[barCharts].setOption(option);
            barCharts++;
          }

          /* This function reads for the filters that have been selected and
          from the options and filters these from the downloaded data*/
          function applyDataFilter(elm){
            var newData = new Array();
            var selectedBoxes = new Array();
            var checkBoxes = (document.getElementsByClassName('checkboxs_filter'));
            for(var i =0 ; i < checkBoxes.length; i++){
              if(checkBoxes[i].checked == true){
                selectedBoxes.push(checkBoxes[i]);
              }
            }
            var filterCat = document.getElementById('filterButton').key;

            for(var i = 0; i < returndata.length; i++){
              for(var j = 0; j < selectedBoxes.length; j++){
                if(selectedBoxes[j].key == returndata[i][filterCat]){
                  newData.push(returndata[i]);
                }
              }
            }

            for(var i =  currentChartCount; i < ChartsArray.length; i++){
              updateExistingCharts(ChartsArray[i], chartSettings[i], newData);
            }



            for(var i = currentMapcount; i < mapsArray.length; i++){
              updateExistingMaps(mapsArray[i], mapSettings[i], newData);
            }

          }

          /* This function will update the charts that are currently on the page*/
          function updateExistingCharts(chart, chartSettings, dataArray){
            var ops = chart.getOption()
            var cat = chartSettings.cat;
            var val = chartSettings.val;

            var catArray = createCategory(cat, dataArray);
            var valArray = createValues(val, catArray, cat, dataArray);


            for(var i =0 ; i < ops.series.length; i++){
              if (ops.series[i].type == 'pie'){
                updatePieChart(chart, dataArray, catArray, valArray);
                return;
              }
            }

            chart.setOption({
                xAxis: [{
                    data: catArray
                }],
                series: [{
                    data: valArray
                }]
            });
          }
          /*if the chart is a pie chart we need to update differently then the other charts*/
          function updatePieChart(chart, dataArray, catArray, valArray){
            var pieData = new Array();

            for(var i=0; i< catArray.length; i++){
              pieData.push({value: valArray[i], name: catArray[i]});
            }
            chart.setOption({
              series:[{
                data:pieData
              }]
            });
          }

          /*Update existing maps, the idea is to remove the existing dots on the map*/
          function updateExistingMaps(map, mapSettings, newData){
            var latArray = new Array();
            var longArray = new Array();
            var latitude = mapSettings.lat;
            var longitude = mapSettings.long;



            mapSettings.layer.remove();
            for (var i  = 0; i < newData.length; i++){
              if(newData[i][latitude] != null){
                latArray.push(newData[i][latitude]);
                //latAve += parseFloat(newData[i][latitude]);
                longArray.push(newData[i][longitude]);
                //longAve += parseFloat(newData[i][longitude]);
              }
            }
            mapSettings.layer = L.layerGroup().addTo(map);
            for (var i=0; i<latArray.length; i++){
              var circle = L.circle([latArray[i], longArray[i]], {
                  color: 'red',
                  fillColor: '#f03',
                  fillOpacity: 0.5,
                  radius: 300
              }).addTo(mapSettings.layer);
              //circle.bindPopup("I am a state code " + pulledData[i]["state_code"]);
            }


          }
